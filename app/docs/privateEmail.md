# General Private Email configuration for mail clients and mobile devices

It is possible to use the service just as ordinary email account and configure mail client to retrieve and send messages through [Namecheap Private Email](https://www.namecheap.com/hosting/email/) server.

Only encrypted connections are supported by our mail servers, so please use the following settings:

**Username**: your email address
**Password**: password for this email account
**Incoming/outgoing servers name**: mail.privateemail.com

**Incoming server type**: IMAP or POP3
**Incoming server (IMAP)**: 993 port for SSL, 143 for TLS/STARTTLS
**Incoming server (POP3)**: 995 port for SSL

**Outgoing server (SMTP)**: 465 port for SSL, 587 for TLS/STARTTLS
Outgoing server authentication should be **switched on**, SPA (secure password authentication) must be disabled.

More specific instructions can be found here.

Namecheap Private Email (Powered by Open-Xchange) User Guide [can be found here](https://www.namecheap.com/support/knowledgebase/subcategory/2175/private-email-client-setup/).

You may also connect your mailbox from the **Windows/Apple/Android** device **directly**. Feel free to [check this guide](https://www.namecheap.com/support/knowledgebase/article.aspx/9665/2178/namecheap-private-email-webmail-features/#connect_dev).