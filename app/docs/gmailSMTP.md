# How do I use Gmail SMTP server to send emails for SRM?

## Purpose

Previously, users who used **Gmail** as their Service provider obtained SMTP authentication via OAuth authorization. However, as of **January 24 2022**, OAuth is no longer supported. A new Gmail authorization must be applied to SRM. Users must update SRM to 1.2.3-8017 or later versions to send emails via Gmail. For users who cannot update SRM, you need to configure Google's SMTP server settings. This article will guide you through the steps of configuring Gmail SMTP server.

## Resolution

### Authorize access to your Google account

1. Sign in to your Google Account.
1. Go to Security > Less secure app access. Click Turn on access.

![step1](./images/resolution_1.png)

![step1](./images/resolution_2.png)

### Set up Gmail SMTP server

1. Sign in to your SRM and go to Control Panel > Notification > Email.
1. Tick the Enable email notifications checkbox.
1. Change Service Provider from Gmail to Custom SMTP server.
1. To connect to Gmail SMTP Server, please fill in the required fields according to the details below:
    - SMTP Server: smtp.gmail.com.
    - SMTP Port: 587
    - Authentication required: Tick the checkbox.
        - Username: Enter your Gmail address.
        - Password: Enter your Google account password. If you have enabled two-step verification (a.k.a., two-factor authentication) for your Google account, refer to this article to generate an application password.
    - Security connection (SSL/TLS) is required: Tick the checkbox.
        - Sender name: Enter a desired name.
        - Sender email: Enter your Gmail address. Please note that if the Sender email differs from the Username above, your emails may be marked as spam.
1. Click Apply to let SRM log in to your Google account.2
1. You can click Send a test email to check if your settings are correct.


> Notes:<br>
    1. Less secure app access is not available for accounts with 2-step verification enabled. Such accounts need an application-specific password to grant SRM access.<br>
    2. If SRM cannot sign in to your Google account, please make sure that you have enabled Less secure app access in advance. If not, please go to your Google account to enable it. Then, go back to Control Panel to fill the Password field under Authentication required before clicking Apply.


