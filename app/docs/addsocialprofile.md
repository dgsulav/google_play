## How can I add a social profile?

Below are the types of profiles you can add to Sociair:

 - Facebook Pages
 - Twitter profiles
 - Instagram business profiles
 - LinkedIn business pages & personal profiles
 - YouTube profiles (channels)

If you want to add social profile, click on the + icon on left-nav and select **Add a social profile**.

![addsocialprofile](./images/addsocialprofile.png)

> Please note you must be an Owner or Manager of your Organization to be able to add social profiles.

You'll arrive on the main Connect page, from where you can add social profiles (you can get there also by clicking [here](https://app.sociair.com/user/login)). From there, choose what type of social profile you'd like to add - Facebook Page or Group, Twitter profile, etc. **When prompted, accept the permissions by clicking on OK**.

![connectPage](./images/connectPage.png)

## Facebook

To add a Facebook page, you need to:

1. Make sure you have the **Admin** or **Editor** role;
1. Identify the page and click on the **Add profile** button. 

## Instagram
To add an Instagram account, please follow these steps:

1. Be sure the Instagram profile you're going to connect with is an **Instagram Business profile** - Instagram personal profiles are no longer supported in Sociair (learn more about how to convert Instagram personal profile to Instagram Business profile [here](https://www.facebook.com/business/help/502981923235522))
1. Also, be sure your Instagram Business profile is linked to a Facebook Page that you have admin permissions on - learn more about this [here](https://help.instagram.com/356902681064399)
1. Then click **Add an Instagram profile** button
1. Accept permissions when prompted
1. Select the correct profile from the list and click **Add profile**

## Twitter
To add a Twitter account, please follow these steps:

1. **[Important]** Be sure you're logged into Twitter with the account you'd like to add
1. Click on the **Add a Twitter profile** button
1. Authorize the app

Wait a few moments to see your inbox synchronized and enjoy!

![connectTwitter](./images/connecttwitter.png)

## LinkedIn

1. Be sure you're logged in with your LinkedIn profile
1. Click **Add a LinkedIn profile**
1. Choose your LinkedIn company page or LinkedIn personal account from the list

> You can either add a LinkedIn Company Page you created (or administer), or you can add a Company Page on which you've been invited as admin. Please note that you will need a [Super Admin role](https://www.linkedin.com/help/linkedin/answer/4783) on the page to add it to Sociair.

## YouTube
1. Click *Add a YouTube channel*
1. Choose the correct Google account you're managing the YouTube profile with
1. Click *Allow* on all permissions asked
1. Choose the profile

> You need to have **Manager** role on YouTube brand account to be able to add it to Sociair. YouTube API currently [doesn't allow](https://support.google.com/youtube/answer/9481328?hl=en) invited users (regardless their role) to add them. You can add a channel listed directly under your Google account, but you can also add one or more channels managed under brand accounts, tied to your Google account. More about managing YouTube channels [here](https://support.google.com/youtube/answer/4642409?hl=en).