## Comments & posts

Click on the item that you want to reply to and type a reply into the provided field. You can add emojis, picture/video or choose among any saved replies. Don't forget to hit **Send** when done!

When adding a picture or a video to the reply, you add one at a time. You can remove and/or replace it anytime before sending. The file size limit for attached image file is 4 Mb, for video file it's 10 Mb.

To reply via PM, just click on the dropdown menu and select Reply in PM. Be aware that you can reply to a given comment/post in private only once and only within 7 days since you've received it - after that you'll only be able to reply the "standard" way (in a comment). 

If you handle comments through Sociair and also directly on Facebook, they might not be marked as reviewed on Agorapulse even if they have been reviewed on Facebook. We technically do not get any information that the comment has already been reviewed from Facebook.

## Messages
To reply to a private message, click on the PM in the Inbox and type in the reply in the provided field. You can add an emoji or use saved replies. Again, don't forget to hit **Send** when done!

You can add different types of attachments:

 - image or video files (standard formats are supported: JPEG, PNG, GIF, MOV, MP4, AVI; max 25 Mb file size)
 - PDF files (max 15 Mb file size)
 - MP3 files (max 9 Mb file size)

Please note you can reply to Facebook PMs up to 7 days (incl.) since you receive them - after the 7 day period you won't be able to reply to a message (Facebook policy).

If you reply to a Facebook private message using Sociair, the message will not be marked as read on Facebook. The thing is, we're technically not able to mark it as read - long story short, there's no permission in the current Facebook API that would allow us to do that. We only have 'read-only' access.

So, for now, even if you reply to a private message, it'll remain unread on Facebook.