# SMTP Configuration settings for Zoho Mail

SMTP or Simple Mail Transfer Protocol allows you to send emails from an email application through a specific server. For example, if you want to use your [Zoho Mail](https://www.zoho.com/mail/) account to send email through another email client, you will need to configure the settings in that client with Zoho's SMTP information.

## SMTP Configuration settings for Zoho Mail - SSL
**Outgoing Server Settings:** (Personal users with an email address, username@zoho.com):

Outgoing Server Name: **smtp.zoho.com**
Port: **465**
Security Type: **SSL** 
Require Authentication: **Yes** 

**Outgoing Server Settings:** (Organization users with a domain-based email address, you@yourdomain.com):

Outgoing Server Name: **smtppro.zoho.com**
Port: **465** with **SSL** or
Port: **587** with **TLS**
Require Authentication: **Yes**

**The email address should match the email address/ [email aliases](https://www.zoho.com/mail/how-to/create-email-alias.html) of the account, for which the authentication details are provided.** 

## SMTP Configuration settings for Zoho Mail - TLS
Outgoing Server Name: **smtp.zoho.com**
Port: **587**
Security Type: **TLS** 

Require Authentication: **Yes. The email address should match the email address/ email aliases of the account, for which the authentication details are provided.** 

You may require an [Application-specific Password](https://www.zoho.com/mail/help/adminconsole/two-factor-authentication.html#alink5) to set up the account on other devices if you've enabled Two-Factor Authentication.

## Duplicate Sent Copies - Do not save a copy in Sent folder

When you send an email from other email clients like Outlook or other email clients using smtp.zoho.com, those emails are automatically saved in your Sent folder. However, there are some email clients who by behavior, save a copy in the local Sent folder. This causes duplicate emails in the Sent folder (one saved by the email client and another by the Zoho Servers). In such cases, the user can choose the option **Do not save a copy in Sent folder** to avoid duplication. ​

1. Login to [Zoho Mail](http://www.zoho.com/mail/login.html)
1. Go to **Settings** ​
1. Navigate to [Mail Accounts](https://mail.zoho.com/zm/#settings/all/mailaccounts) and click the respective email address from the left listing.
1. Under the **SMTP** section, uncheck the **'Save Sent Mail Copy'** to not save the emails sent using the smtp.zoho.com configuration in the Sent folder.

![zoho_1](./images/zoho_1.jpg)