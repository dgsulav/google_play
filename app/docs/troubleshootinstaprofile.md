# Troubleshooting Instagram Business Profiles

If you're trying to connect an Instagram Business profile, and you're not seeing it populate in your list of profiles, there are a few things to check which may help troubleshoot this issue and help you get your Instagram profile connected.

![Trouble_shooting_Insta_BusinessProfile](./images/trouble_shooting_Insta_BusinessProfile.png)

## Make sure it's an Instagram Business profile

First, please ensure that the Instagram profile is actually a **business profile**.

- Instagram personal profiles are not supported on Sociair or any other 3rd party tool.
- You'll be able to benefit from advanced features, such as direct publishing without the use of the mobile app, reporting & insights, or real-time updates of the Inbox, just to name a few.

[Here's](https://www.facebook.com/help/502981923235522) how to set up a business profile (or switch from personal to business).

## Link Facebook Page
> Please ensure that you link the Facebook Page with the Instagram Business Profile using the Instagram mobile app.

It's [necessary](https://developers.facebook.com/docs/instagram-api/overview#pages) to connect the Instagram Business Profile with a published Facebook Page. If your Instagram Business Profile is not linked with a Facebook Page yet, you'll need to:



1. Open Instagram mobile app

2. On your Instagram profile, choose **Edit profile**

![Insta_EditProfile](./images/Insta_EditProfile.png)

3. Then, under Public business information tap **Connect or Create**

![Insta_ConnectCreate](./images/Insta_ConnectCreate.png)

4. Then tap **Connect existing Page** and choose one of the listed Facebook Pages. Otherwise, if you don't have a Page choose **Create** and create a new one.

![Insta_ConnectExistingPage](./images/Insta_ConnectExistingPage.png)

5. Tap Done when you've chosen your Page

![Insta_Done](./images/Insta_Done.png)

In case your Instagram Business Profile is linked to the wrong Facebook Page you can choose another one or create a completely new one - to do that, you need to:

1. Go to Edit profile again

1. Then, under Public business information tap on currently linked Facebook Page

1. Choose another Facebook Page or create a new one

In case you don't see any Facebook Pages to choose from, you might want to convert your Instagram Business Profile to a personal profile and then back to a Business profile again - please jump [here](#Troubleshooting-Instagram-Business-Profiles) to see how to do that.

## Check your Page role

> Please make sure you're connecting with your own Facebook account and not someone else's.


Please be sure that you have the correct page role to manage the Facebook Page that is linked to your Instagram Business profile (that being said, you must be **Admin**). [Here's](https://www.facebook.com/help/187316341316631) how you can check each role and what permission level is associated with it. You have to:

1. Visit facebook.com and log in with your own Facebook account
1. Go to the Facebook Page which is linked to your Instagram
1. Click Settings
1. Choose Page roles

![checkpagerole](./images/checkpagerole.png)

## Ensure you accept the permissions

Sometimes we might not have the right permissions to list the Instagram business profile(s)/Facebook Pages - to make sure we have everything we need please do the following:

Log in to Sociair
Click **List my business profiles**
Click **Reconnect with Facebook**
Accept all permissions


> NOTE: Please do not manipulate individual permissions via the Choose what to allow link - this won't authorize your Facebook account correctly and we won't be able to list your Instagram Business Profiles. Please accept all permissions by clicking on the OK button.
![choosewhattoallow](./images/choosewhattoallow.png)



## Switching back to a personal profile and then to Business Profile again 

Sometimes Instagram doesn't detect the switch from a personal profile to a business profile. In this case, it will be necessary to switch the profile back to personal and then revert again to business profile - [here's](https://www.facebook.com/help/instagram/1717693135113805) how to switch back to personal:

1. Go to your profile
1. Tap Settings
1. Tap Account
1. Tap Switch to Personal Account
1. Tap Switch to Personal to confirm

> Note: Please note that you'll lose your Instagram Insights when making this change.


**Account still not listed?** Let us know as many details as possible, including what steps you've completed while troubleshooting this.  Screenshots of the linked Instagram account and Facebook pages are always helpful and appreciated. As always, our support team is happy to help!