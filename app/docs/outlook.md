# Outlook SMTP Settings

Here are the details that you’ll need to configure your email client or WordPress website to use the Outlook SMTP server:

1. **Server Address:** smtp-mail.outlook.com
1. **Username:** Your Outlook Email Address (e.g. example@outlook.com)
1. **Password:** Your Outlook Password
1. **Port Number:** 587 (With TLS)
1. **Alternative Port Number:** 25 (Without TLS/SSL)
1. **Sending Limits:** 300 Emails a day or 100 recepients a day.