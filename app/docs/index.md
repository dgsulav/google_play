# Connecting your Google Play Developer Console to Sociair

To get the most from Sociair, it’s a good idea to link your Google Play Developer Console account and authorize view access. Linking your Developer Console to Sociair allows you to:

- get app version data for each app review
- get device data for each app review
- fetch the complete review history for your Google Play apps.
- fetch the complete rating history and get ratings breakdowns.
- reply from within Sociair (limited beta)

In this guide we’ll cover the procedure to link your Google Play Developer Console account to Sociair and how to authorize access to your reviews.

### Important – You need to be the Google Play Developer Console account owner to complete this guide.

#### Setting Up A New Google Play Developer Console Connection

**Step 1 – Navigate to ‘Developer Account’ and select ‘API access’.**

![step1](./images/step_1.webp)

**Step 2 – Navigate to ‘Developer Account’ and select ‘API access’.**

![step2](./images/step_2.webp)

**Step 3 – If you see the ‘Linked Google Cloud project’ section as below, click on ‘Create new project’ button. If you do not, skip to Step 4.**

![step3](./images/step_3.webp)

**Step 4 –  Click on the ‘View project’ link under Linked Google Cloud project**

![step4](./images/step_4.webp)

**Step 5 – On the dashboard click “ENABLE API AND SERVICES”**

![step5](./images/step_5.webp)

**Step 6 – Search for the following API’s and make sure they are all ENABLED:**

- **Google Cloud APIs** (lets us authenticate you)
- **Google Cloud Storage** (allows us to access historical review data)
- **Google Cloud Storage JSON API** (also allows us to access historical review data)
- **Google Play Android Developer** (allows us to access current review data)

![step6](./images/step_6.webp)

**Step 7 – Back in your developer console, press ‘CREATE SERVICE ACCOUNT’.**

![step7](./images/step_7.webp)

**Step 8 – Click on ‘Google API Console’ from the popup that appears.**

![step8](./images/step_8.webp)

**Step 9 – When your API console loads, click ‘CREATE SERVICE ACCOUNT’ at the top.**

![step9](./images/step_9.webp)

**Step 10 – Give the service account a name (this can be anything you want, but it’s a good idea to name it something to do with Sociair so you know what it is in future), and then click ‘Create’.**

![step10](./images/step_10.webp)

**Step 11 – Under the ‘Select a role’ dropdown, choose ‘Basic’ and select ‘Browser’, then click ‘Done’.**

![step11](./images/step_11.webp)

**Step 12 – Click the three dots and then ‘Create key’, and in the pop-up make sure ‘JSON’ is selected and click** ‘Create’.

![step12a](./images/step_12_a.webp)

![step12b](./images/step_12_b.webp)

**Step 13 – You should then get a message telling you ‘Private key saved to your computer’ and the private key should begin downloading. Click ‘Close’ on the dialog, then click ‘Done’ on the page.** (Clicking the Done button on the dialog back in your developer console will refresh the page and show your what has changed)

![step13](./images/step_13.webp)

**Step 14 – Back in your Developer Console under ‘API access’ you should now see the Sociair service account you created under the list of service accounts.  Click the ‘GRANT ACCESS’ button next to the Sociair service account you just created.**

![step14](./images/step_14.webp)

**Step 15 –  Account Permissions**
Uncheck all except:

1. **‘View app information and download bulk reports (read-only)‘** – so you’re only giving the Visibility permission to Sociair. This allows us to access the data in the Reports section of your Console account, which is where we obtain the historical review data.
1. **‘Reply to reviews‘** – so you can reply from within Sociair.

Then press **‘Invite User’**.

![step15a](./images/step_15_a.webp)

![step15b](./images/step_15_b.webp)

**Step 16 – Login to your Sociair account, in the header, click the Cog icon, then click ‘Link App Stores’ from the menu. On the page that loads, press ‘Link a Google Account’.**

![step16](./images/step_16.webp)

**Step 17 – Click ‘Choose File’.**

![step17](./images/step_17.webp)

**Step 18 – Select the private key that was downloaded earlier.**

![step18](./images/step_18.webp)

**Step 19 – Click on the ‘Upload Service Account Key’ button.**

![step19](./images/step_19.webp)

**Step 20 – Head back to your Play Developer Console and navigate to “Download Reports” from the left menu. Click into “Reviews” and select your app from the right.**

![step20](./images/step_20.webp)

**Step 21 – Click on “Copy Cloud Storage URI”**

![step21](./images/step_21.webp)

**Step 22 – Back in Sociair, paste the URIs you just copied in and press “Start Fetching”**

![step22](./images/step_22.webp)

```
 NOTE: Google is currently taking up to 24 hours for the permissions to take effect. If you get an error and are confident all steps above have been completed correctly, then please try again from step 15 in 24 hours.
 ```

**Step 23 – Once it’s loaded you’ll see a message saying “Our bots are completing the account link” and you should see the Authenticated badge on each authenticated app on the Manage Sources page soon.**

![step23](./images/step_23.webp)

**Step 24 – Enable replies**

NOTE: This is currently limited beta functionality for some paid Sociair customers. If you don’t see this option and would like to like to test it please [reach out](http://sociair.com
"Sociair")  to us.
<br>Back on the Linked Accounts list press on ‘Enable Replies’ next to the Google Service you just added.

![step24a](./images/step_24_a.webp)

![step24b](./images/step_24_b.webp)

**Step 25 – Once the new data is fetched you can navigate to the [Reviews Page](http://sociair.com
"Sociair review"). You will now see extra metadata on reviews for App Version, Device and OS Version.**

![step25a](./images/step_25_a.webp)

If you enabled replies then you will see a new option to reply from within Sociair:

![step25b](./images/step_25_b.webp)

![step25c](./images/step_25_c.webp)

#### Adding Replies To An Existing Linked Account

NOTE: This is currently limited beta functionality for some paid Sociair customers. If you don’t see this option and would like to like to test it please [reach out](http://sociair.com
"Sociair") to us.

**Step 1 – Log in to your Google Developer Console account, choose ‘API access’ and then ‘View access’ on your service account.**

![stepa](./images/step_A.webp)

**Step 2 – Select ‘Account permissions’**

![stepb](./images/step_b.webp)

**Step 3 – Enable ‘Reply to reviews’ and click ‘Save changes’**

![stepc](./images/step_c.webp)

**Step 4 – Go to ‘Step 24’ above to enable replies in Sociair**

If you need any further assistance on linking the Google Play developer console, don’t hesitate to contact us at support@sociair.com with any questions or check out our [FAQ section here](http://sociair.com
"FAQ").­­­­

### Who Can Access This Data?

The data collected under the authenticated Google Play app (the version with the green padlock) can only be accessed by users associated with teams that have a linked Google Play Developer Console account associated with that app connected to their team.
