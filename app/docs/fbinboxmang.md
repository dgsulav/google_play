There are plenty of actions you can do in the INBOX, to keep it clean on the one hand, but not to miss any conversation with your fans, on the other.

## 1. Review

Hover your mouse over any post and click the **Review** icon to show every other team member that this item has been handled by and didn't slip through the cracks.

All posts/comments/messages that have **not** been reviewed by any admin can be found in the **Inbox > To review** tab.

You can also review all items, by one single click, thanks to the **Review all** button, or mark only certain items (bulk) you want to review, and then hit the **Review selected items** button.

## 2. Label

You can label any item for easy identification - click on an item, switch to **label** and type in/select label(s). You can then quickly find all items labeled with a given label via label filters.

Allowed characters for labels usage:

 - lowercase and uppercase characters
 - space
 - colon ```:```
 - asterisk ```*```
 - slash ```/``` and backslash ```\```
 - dash ```-```
 - plus sign ```+```
 - underscore ```_```
 - at sign ```@```
 - hashtag sign ```#```
 - and sign ```&```
 
You can remove the label at the same place where you add them - just click the X icon. Labels already created and used before can be erased from the list of labels by erasing each and every label - in other words, to remove a label , you need to erase that particular label from each item it was assigned on. Once a label is not assigned to any item, it'll be removed.

## 3. Bookmark
Bookmarking an item is another way how to easily find what you're looking for. As in the web browser, bookmark comment, message or post, to quickly access it. 

## 4. Assign
You can assign any fan content on your Page to any admin by clicking on the content, then on the **Assign**. You will then see a note "Assigned to {name of the admin}" below the item.

You can also add an optional note when assigning an item. This could be useful for internal communication among team members.

Once the content has been assigned, the **admin will receive an instant notification email** informing them a post has been assigned to him/her.

It is only possible to assign content to one admin at a time. Each admin has an 'Assigned to me' filter on the top of the view for quick and easy access.

## 5. Like
You can like a user comment or post by clicking on it and then hitting the **Like** icon. You can undo the like action anytime by hitting the same icon again.


> NOTE: It's not possible to like/unlike Facebook Reviews, due to ongoing technical issue on Facebook's side. 


## 6. Hide
You can easily hide posts or comments from your Page. Simply click the post/comment and hit the **Hide** option.

You can undo the Hide action anytime - just click the Unhide icon.

## 7. Remove
To remove (delete) a post or comment, click the item you want to remove and click on **Remove**. You will be asked to confirm your action.

When a post or comment is removed from Agorapulse, it's also permanently removed from the Facebook page.

> **IMPORTANT NOTE**: The remove action is permanent, meaning it's irreversible. This is why we put the confirmation pop-up when removing an item, to ensure you are 100% confident you wish to remove that item.

You can also remove a private message - either single message or entire conversation thread. This will remove the message(s) from Sociair only - nothing will be deleted from Facebook.