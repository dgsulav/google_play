# Connect a Facebook Group

Before adding a Facebook Group in Sociair, **please be sure to check that you are an Admin of the Group** - you'll need to authenticate with the Facebook profile which has an Admin role in the Facebook Group settings. 

Simply being a member of a Group is not sufficient to make the Facebook Group work in Sociair. If you are not an Admin of a Group you want to add, please ask another Admin to give you the relevant role first.

In order to add a Facebook Group, please follow these steps:

1.  Click on + button of your screen. Then choose Add a social profile.

1. Before proceeding, please make sure you have an Admin role with the Facebook profile you're going to authenticate with. Accept required permissions by clicking on Facebook Group.

1. We'll list all the Facebook Groups on which you have an Admin role. Click on the + Add button next to the Facebook Group that you want to add.

1. In this step, you'll now need to authorize Sociair in Facebook Group Settings directly - click on the Authorize Sociair on Facebook button and you'll be redirected to your Facebook Group settings.

1. Scroll to the bottom of the screen and click to edit the Apps settings. Continue by clicking on Add Apps and then search for "Sociair" on the left side - choose Sociair from the selection on the right. Confirm this by clicking Add.

1. You'll be informed that the Sociair was added to your Facebook Group settings and Sociair will be listed in the Group Apps.

1. Now you can return to Sociair - after you reload the page you'll be able to publish to your FB Group.

