# Viber Bot Configuration

In order to implement the API you will need the following:

1. **An Active Viber account** on a platform which supports bots (iOS/Android). This account will automatically be set as the account administrator during the account creation process.
1. **Active bot** - Create a bot [here](https://partners.viber.com/login). Note: an active Viber account is needed to receive the login code to access the bot creation page.
![viberbot1](./images/viber_1.png)
1. **Account authentication token** - unique account identifier used to validate your account in all API requests. Once your account is created your authentication token will appear in the account’s “edit info” screen (for admins only). Each request posted to Viber by the account will need to contain the token.
![viberbot2](./images/viber_2.png)

**Supported platforms**

Bots are currently supported on iOS and Android devices running Viber version 6.5 and above and on desktop from version 6.5.3.