# Connecting Instagram business profile

If you have Instagram personal profile and wish to convert it to Instagram business profile, please refer to [this article](https://help.instagram.com/502981923235522).

Why you should convert your Instagram personal profile to a Instagram business profile? What are the benefits?

You'll be able to:

 1. publish directly without the need to use mobile app
 1. view incoming comments into your Inbox in real-time
 1. use advanced inbox features, which were not available previously (threaded comments, hide/delete feature, etc.)

To connect your Instagram business profile in Sociair:

1. Click on + in left-side navigation bar, then choose Add a profile

1. Click Add an Instagram profile.

1. Click OK when asked to grant permissions. We need to ask for the permissions because you're going to use a new way of direct publishing to Instagram, via Facebook.

1. Click Add profile next to the business profile you'd like to add

If you have a business profile, but it's still not listed, please [refer here](/troubleshootinstaprofile).

In case you need help, you can always contact us.

Once you have connected your Instagram business profile(s) in Sociair, you can publish directly on Instagram - it works the same way as publishing to any other social profile.

If you do not wish to use direct publishing method, you still can continue using the push notifications publishing method, with finishing your post on mobile device. This method can be used also after you have enabled direct publishing on your profile - you just need to disable it in **Profile settings > General**.